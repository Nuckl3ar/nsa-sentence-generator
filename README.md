What is this?
=============

After hearing General Alexander at BlackHat2013 I decided to help him as he requested writing this script that construct a good speech from the scratch.

What can I do with this?
========================

You can figure out what Gen. Alexander would say in any situation when he is asked about PRISM. Also you can put this in a webserver and allow every american politician build their speeches.

How does it work?
=================

This sentence generator takes a list of wildcards used by american politicians and fills the rest of the sentence with useless words, as you can see this awesome advanced IA is similar to the reality.

OUTPUT EXAMPLE:
---------------

![Gen. Alexander says](http://i.imgur.com/IoGa4o7.png)