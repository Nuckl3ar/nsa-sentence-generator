#! /usr/bin/python
import random, os, sys

if sys.platform == 'win32':
	os.system('cls')
else:
	os.system('clear')


print '                             ;;\\/;;;;;;;;'
print '                          ;;;;;;;;;;;;;;;;;'
print '                       ;;;;;;;;;;;;     ;;;;;'
print '                      ;;;;;    ;;;         \;;'
print '                     ;;;;;      ;;          |;'
print '                    ;;;;         ;          |'
print '                    ;;;                     |'
print '                     ;;                     )'
print '                      \    ~~~~ ~~~~~~~    /'
print '                       \    ~~~~~~~  ~~   /'
print '                     |\ \                / /|'
print '                      \\| %%%%%    %%%%% |//'
print '                     [[====================]]'
print '                      | |  ^          ^  | |'
print '                      | | :@: |/  \| :@: | |'
print '                       \______/\  /\______/'
print '                        |     (@\/@)     |'
print '                       /                  \ '
print '                      /  ;-----\  ______;  \ '
print '                      \         \/         /'
print '                       )                  ('
print '                      /                    \ '
print '                      \__                  /'
print '                       \_                _/'
print '                        \______/\/\______/'
print '                         _|    /--\    |_'
print '                        /%%\  /""""\  /%%\ '
print '         ______________/%%%%\/\'"''/\/%%%%\______________ '
print '        / :  :  :  /  .\%%%%%%%\""/%%%%%%%/.  \  :  :  : \ '
print '       )  :  :  :  \.  .\%%%%%%/""\%%%%%%/.  ./  :  :  :  ( '
print '\n'

word = "blah"
wildcards = ["save american lives", "defend our country","our soldiers", "america", "freedom", "terrorism", "terrorists", "war on terror", "we are in danger", "safety", "nine eleven", "living among us", "we and our allies"]
selection, nums, final = ([] for i in range(3))
rand = ''
data = ''

def sel_words():
	for i in randomize():
		selection.append(wildcards[i])
	return selection

def randomize():
	while True:
		rand = random.randint(0,(len(wildcards)-1))
		if rand not in nums:
			nums.append(rand)
		if len(nums) == 3:
			return nums
	
	

for w in sel_words(): 
	for i in [word for i in range(random.randint(2,5))]:
		final.append(i)
	final.append(w)
for w in final:
	data += w + ' '
print '"', data.capitalize(),'" , Keith B. Alexander (Head of NSA)' 


